import os
from http.client import HTTPConnection

from dotenv import load_dotenv

load_dotenv()
router_address = os.environ.get('router_address', '10.0.0.1')  # PTCL Router
host = os.environ.get('host', '172.16.0.1')  # gate 1
username = os.environ.get('username', 'admin')  # gate 1 username
password = os.environ.get('password', 'admin')  # gate 1 password


def get_session_id():
    connection = HTTPConnection(router_address)
    connection.request('GET', '/resetrouter.html')
    response = connection.getresponse()
    response_html = response.read().decode("utf-8")
    session_id = re.search("sessionKey='(\d*)'", response_html).group(1)
    return session_id


def restart_router(session_id):
    connection = HTTPConnection(router_address)
    connection.request('GET', f'/rebootinfo.cgi?sessionKey={session_id}')
    response = connection.getresponse()
    response.read().decode("utf-8")
    print('PTCL rebooting')


"""Basic python based command line tool to control TP-Link
router TL-WR841N/TL-WR841ND (FW 3.16.9)
"""
import sys
import hashlib
import base64
import re
import requests


class TPyLink(object):
    """Basic command line remote control for TP-Link router TL-WR841N/TL-WR841ND (FW 3.16.9)

    Attributes:
        host: hostname or IP address.
        username: admin username.
        password: admin password.
    """

    LOGIN_URL = "http://{0}/userRpm/LoginRpm.htm?Save=Save"
    LOGOUT_URL = "http://{0}/{1}/userRpm/LogoutRpm.htm"
    STATUS_URL = "http://{0}/{1}/userRpm/StatusRpm.htm"
    REBOOT_URL = "http://{0}/{1}/userRpm/SysRebootRpm.htm"
    AUTH_KEY_RE = r"http\://[0-9A-Za-z.]+/([A-Z]{16})/userRpm/Index.htm"

    def __init__(self, host="192.168.0.1", username="admin", password="admin"):
        self.host = host

        md5 = hashlib.md5()
        md5.update(password.encode('utf-8'))
        password = md5.hexdigest()
        import urllib.parse
        s = f"Basic {base64.b64encode(f'{username}:{password}'.encode('utf-8')).decode()}"
        auth = urllib.parse.quote(s)
        self.auth_cookie = {'Authorization': auth}
        self.key = TPyLink.__login__(self)

    def __login__(self):
        """ Try to log in to router. Response should look like
        <body><script language="javaScript">
        window.parent.location.href = "http://192.168.0.1/GQWYNKXBCTLGHPRC/userRpm/Index.htm";
        </script></body></html>
        """
        resp = requests.get("http://{0}".format(
            self.host))
        if resp.status_code != 200:
            print("Response from {0} not OK.".format(self.host))

        resp = requests.get(self.LOGIN_URL.format(
            self.host), cookies=self.auth_cookie)

        auth_key_match = re.search(self.AUTH_KEY_RE, resp.text)

        if auth_key_match:
            key = auth_key_match.group(1)
            return key
        else:
            print("Login error. No authentification key found in response.")
            print(resp.text)

    def reboot(self):
        """ Reboot the router """
        if not self.key:
            print("No key present. Login first.")
            return

        resp = requests.get(self.REBOOT_URL.format(self.host, self.key) + "?Reboot=Reboot",
                            cookies=self.auth_cookie,
                            headers={'referer': self.REBOOT_URL.format(self.host, self.key)}
                            )
        if resp.status_code != 200:
            print("Reboot error")
        else:
            print("Router rebooting")
            import time
            time.sleep(5)

    def __logout__(self):
        requests.get(self.LOGOUT_URL.format(self.host, self.key),
                     cookies=self.auth_cookie,
                     headers={'referer': self.STATUS_URL.format(self.host, self.key)}
                     )


def main(argv):
    """Basic command line remote control for TP-Link router TL-WR841N/TL-WR841ND (FW 3.16.9)"""

    get_traffic = False
    reboot = True

    tpy = TPyLink(host=host, username=username, password=password)
    if reboot:
        tpy.reboot()


if __name__ == '__main__':
    restart_router(get_session_id())
    main(sys.argv[1:])
